package logs.controllers;



import ch.qos.logback.classic.Level;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.client.RestTemplate;

import java.util.Date;


@RestController
public class LogController {

    @Autowired
    RestTemplate restTemplete;

    @Bean
    RestTemplate restTemplate() {
        return new RestTemplate();
    }

    private static final Logger LOG = LoggerFactory.getLogger(LogController.class.getName());
    private static final Logger logger
            = LoggerFactory.getLogger(LogController.class.getName());

    @RequestMapping(value = "/logdemo")
    public String helloWorld() {
        String response = "Hello user ! " + new Date();
        for (int i = 0; i < 100; i++) {
            logger.info("Example log from {}", LogController.class.getSimpleName());
        }

            return response;
    }
    @RequestMapping(value = "/contex")
    public String helloWorld1() {
        String response = "Hello user 111111! " + new Date();
        ch.qos.logback.classic.Logger parentLogger =
                (ch.qos.logback.classic.Logger) LoggerFactory.getLogger("logback");

        parentLogger.setLevel(Level.INFO);

        Logger childlogger =
                (ch.qos.logback.classic.Logger)LoggerFactory.getLogger("logback.tests");

        parentLogger.warn("This message is logged because WARN > INFO.");
        parentLogger.debug("This message is not logged because DEBUG < INFO.");
        childlogger.info("INFO == INFO");
        childlogger.debug("DEBUG < INFO");

        return response;}
}
